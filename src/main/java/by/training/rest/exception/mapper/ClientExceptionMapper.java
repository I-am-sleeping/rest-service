package by.training.rest.exception.mapper;

import javax.ws.rs.ClientErrorException;
import javax.ws.rs.core.Response;
import javax.ws.rs.ext.ExceptionMapper;

public class ClientExceptionMapper
        implements ExceptionMapper<ClientErrorException> {
    @Override
    public Response toResponse(ClientErrorException e) {
        return Response.status(e.getResponse().getStatus())
                .entity(e.getMessage()).build();
    }
}
