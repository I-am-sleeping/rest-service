package by.training.rest.model;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class DocumentDao {
    private static DocumentDao instance = null;
    private Map<Long, Document> storage;
    private long nextId = 6;

    private DocumentDao() {
        init();
    }

    private void init() {
        storage = new HashMap<Long, Document>();
        for (long i = 1; i < nextId; i++) {
            Document tmp = new Document();
            tmp.setId(i);
            tmp.setName("name" + i);
            List<Chapter> chapters = new ArrayList<Chapter>();
            for (int j = 1; j < 4; j++) {
                Chapter chapter = new Chapter();
                chapter.setId(j);
                chapter.setNumber(j);
                chapter.setPages(10 * j);
                chapters.add(chapter);
            }
            tmp.setChapters(chapters);
            storage.put(i, tmp);
        }
    }

    public static DocumentDao getDocumentDao() {
        if (instance == null) {
            instance = new DocumentDao();
        }
        return instance;
    }

    public List<Document> read() {
        return new ArrayList<Document>(storage.values());
    }

    public Document read(long id) {
        return storage.get(id);
    }

    public void create(Document doc) {
        long id = nextId;
        nextId++;
        doc.setId(id);
        storage.put(id, doc);
    }

    public void update(long id, Document doc) {
        doc.setId(id);
        storage.put(id, doc);
    }

    public void delete(long id) {
        storage.remove(id);
    }
}
