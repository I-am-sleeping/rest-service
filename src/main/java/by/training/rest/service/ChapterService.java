package by.training.rest.service;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import by.training.rest.model.Chapter;

@Path("/documents/{documentId}/chapters")
public interface ChapterService {
    @PUT
    @Path("/{chapterId}")
    @Consumes(MediaType.APPLICATION_XML)
    Response update(@PathParam("documentId") long documentId,
            @PathParam("chapterId") long chapterId, Chapter chapter);

    @DELETE
    @Path("/")
    Response delete();
}
