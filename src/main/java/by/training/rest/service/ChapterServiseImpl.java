package by.training.rest.service;

import java.util.List;

import javax.ws.rs.ClientErrorException;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import org.springframework.stereotype.Service;

import by.training.rest.model.Chapter;
import by.training.rest.model.Document;
import by.training.rest.model.DocumentDao;

@Service("chapterService")
public class ChapterServiseImpl implements ChapterService {

    private DocumentDao documents = DocumentDao.getDocumentDao();

    @Override
    public Response update(long documentId, long chapterId, Chapter chapter) {
        chapter.setId(chapterId);
        Document doc = documents.read(documentId);
        List<Chapter> chapters = doc.getChapters();
        for (int i = 0; i < chapters.size(); i++) {
            if (chapters.get(i).getId() == chapterId) {
                chapters.set(i, chapter);
                return Response.status(Status.NO_CONTENT).build();
            }
        }
        return Response.serverError().entity("No such chapter").build();
    }

    @Override
    public Response delete() {
        throw new ClientErrorException("Can't delete all Document's Chapters",
                Status.FORBIDDEN);
    }

}
