package by.training.rest.service;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import by.training.rest.model.Document;

@Path("/documents")
public interface DocumentService {

    @GET
    @Path("/")
    @Produces(MediaType.APPLICATION_JSON)
    Response read();

    @GET
    @Path("/{documentId}")
    @Produces(MediaType.APPLICATION_JSON)
    Response read(@PathParam("documentId") long id);

    @POST
    @Path("/")
    @Consumes(MediaType.APPLICATION_XML)
    Response create(Document doc);

    @PUT
    @Path("/")
    @Consumes(MediaType.APPLICATION_XML)
    Response update(Document doc);

    @PUT
    @Path("/{documentId}")
    @Consumes(MediaType.APPLICATION_XML)
    Response update(@PathParam("documentId") long id, Document doc);

    @DELETE
    @Path("/{documentId}")
    Response delete(@PathParam("documentId") long id);

}
