package by.training.rest.service;

import java.util.List;

import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import org.springframework.stereotype.Service;

import by.training.rest.model.Document;
import by.training.rest.model.DocumentDao;

@Service("documentService")
public class DocumentServiceImpl implements DocumentService {

    private DocumentDao dao = DocumentDao.getDocumentDao();

    @Override
    public Response read() {
        List<Document> entity = dao.read();
        return Response.ok(entity).build();
    }

    @Override
    public Response read(long id) {
        Document entity = dao.read(id);
        return Response.ok(entity).build();
    }

    @Override
    public Response create(Document doc) {
        dao.create(doc);
        return Response.status(Status.CREATED).build();
    }

    @Override
    public Response update(Document doc) {
        dao.create(doc);
        return Response.status(Status.CREATED).build();
    }

    @Override
    public Response update(long id, Document doc) {
        dao.update(id, doc);
        return Response.status(Status.NO_CONTENT).build();
    }

    @Override
    public Response delete(long id) {
        dao.delete(id);
        return Response.status(Status.NO_CONTENT).build();
    }

}
